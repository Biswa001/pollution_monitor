import React, { Component } from 'react';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import './App.css';
import Dashboard from './Components/Dashboard.jsx';
import Archive from './Components/Archive.jsx';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Router>
          <div>
            <Route exact path="/" component={Dashboard}/>
            <Route exact path="/archive" component={Archive}/>
          </div>
        </Router>
      </div>
    );
  }
}

export default App;