import React, { Component } from 'react';
import './menu.less';
import { Link } from 'react-router-dom';
import { Layout, Menu, Icon, Badge, Drawer } from 'antd';

const { Header, Sider } = Layout;
const SubMenu = Menu.SubMenu;

const MenuItemGroup = Menu.ItemGroup;

class Head extends Component {
	state = {
	collapsed: true,
	visible: false,
	};

	onCollapse = (collapsed) => {
	this.setState({ collapsed });
	};

	showDrawer = () => {
		this.setState({
			visible: true,
		});
	};

	onClose = () => {
		this.setState({
			visible: false,
		});
	};

	handleClick = (e) => {
		console.log('click ', e);
		this.setState({
			current: e.key,
		});
	}

	render() {
	return (
		<Layout>
			<div>
				<Header className="header mobile-hidden1" style={{ position: 'fixed', zIndex: 10, width: '100%' }}>
					<Menu
						onClick={this.handleClick}
						theme="light"
						mode="horizontal"
						style={{ lineHeight: '64px' }}
					>
						<SubMenu title={<Link to="#"><Icon type="notification" />Notifications<Badge count={2}/></Link>}>
							<Menu.Item key="1">SO2 of CEMS - 1 above warning limit.</Menu.Item>
							<Menu.Item key="2">SO2 of CEMS - 1 above warning limit.</Menu.Item>
						</SubMenu>
						{/*<Menu.Item key="1"><Link to="#"><Icon type="notification" />Notifications<Badge count={5}/></Link></Menu.Item>*/}
						<Menu.Item key="1"><Link to="#"><Icon type="logout" />Logout</Link></Menu.Item>
					</Menu>
				</Header>
			</div>
			
			<div>
				<Header className="header mobile-show1" style={{ position: 'fixed', zIndex: 10, width: '100%' }}>
					<Icon className="menu-icon" type="menu-unfold" onClick={this.showDrawer} />
					<p className="hed-text">Aurassure</p>
					<Menu
						onClick={this.handleClick}
						theme="light"
						mode="horizontal"
						style={{ lineHeight: '64px' }}
					>
						<SubMenu title={<Link to="#"><Icon type="notification" /><Badge dot/></Link>}>
							<Menu.Item key="1">SO2 of CEMS - 1 above warning limit.</Menu.Item>
							<Menu.Item key="2">SO2 of CEMS - 1 above warning limit.</Menu.Item>
						</SubMenu>
						<Menu.Item className="pad-lr-0" key="1"><Link to="#"><Icon type="logout" /></Link></Menu.Item>
					</Menu>
				</Header>
			</div>
		</Layout>
	);
	}
}

export default Head;