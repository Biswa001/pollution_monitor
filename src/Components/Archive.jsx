import React from 'react';
import { Layout, Row, Col, Button, Select, Icon, Tabs, Radio, DatePicker, Checkbox } from 'antd';
import './archive.less';
import Head from './Head.jsx';
import Side from './Side.jsx';

const { Content } = Layout;
const Option = Select.Option;
const RadioGroup = Radio.Group;
const CheckboxGroup = Checkbox.Group;
const aqi = ['aqi1', 'aqi2', 'aqi3'];
const weather = ['weather1', 'weather2', 'weather3'];
const other = ['other1', 'other2', 'other3'];

const city = [];
for (let i = 1; i < 12; i++) {
	city.push(<Option key={'City - ' + i}>{'City - ' + i}</Option>);
}

const device = [];
for (let i = 1; i < 12; i++) {
	device.push(<Option key={'Device - ' + i}>{'Device - ' + i}</Option>);
}

function handleChange(value) {}

function onChange(value, dateString) {
	console.log('Selected Time: ', value);
	console.log('Formatted Selected Time: ', dateString);
}

function onOk(value) {
	console.log('onOk: ', value);
}


class Archive extends React.Component {

	state = {
		value1: 1,
		value2: 1,
		value3: 1,
		value4: 1,
		startValue: null,
		endValue: null,
		endOpen: false,
	};

	disabledStartDate = (startValue) => {
		const endValue = this.state.endValue;
		if (!startValue || !endValue) {
			return false;
		}
		return startValue.valueOf() > endValue.valueOf();
	}

	disabledEndDate = (endValue) => {
		const startValue = this.state.startValue;
		if (!endValue || !startValue) {
			return false;
		}
		return endValue.valueOf() <= startValue.valueOf();
	}

	onChange = (field, value) => {
		this.setState({
			[field]: value,
		});
	}

	onStartChange = (value) => {
		this.onChange('startValue', value);
	}

	onEndChange = (value) => {
		this.onChange('endValue', value);
	}

	handleStartOpenChange = (open) => {
		if (!open) {
			this.setState({ endOpen: true });
		}
	}

	handleEndOpenChange = (open) => {
		this.setState({ endOpen: open });
	}

	onChangeAqi = (checkedList) => {
		this.setState({
			checkedList,
			indeterminate: !!checkedList.length && (checkedList.length < aqi.length),
		});
	}

	onCheckAllChangeAqi = (e) => {
		this.setState({
			checkedList: e.target.checked ? aqi : [],
			indeterminate: false,
			checkAll: e.target.checked,
		});
	}

	onChangeWeather = (checkedList) => {
		this.setState({
			checkedList,
			indeterminate: !!checkedList.length && (checkedList.length < weather.length),
		});
	}

	onCheckAllChangeWeather = (e) => {
		this.setState({
			checkedList: e.target.checked ? weather : [],
			indeterminate: false,
			checkAll: e.target.checked,
		});
	}

	onChangeOther = (checkedList) => {
		this.setState({
			checkedList,
			indeterminate: !!checkedList.length && (checkedList.length < other.length),
		});
	}

	onCheckAllChangeOther = (e) => {
		this.setState({
			checkedList: e.target.checked ? other : [],
			indeterminate: false,
			checkAll: e.target.checked,
		});
	}

	onChange1 = (e) => {
		console.log('radio checked', e.target.value);
		this.setState({
			value1: e.target.value,
		});
	}
	onChange2 = (e) => {
		console.log('radio checked', e.target.value);
		this.setState({
			value2: e.target.value,
		});
	}
	onChange3 = (e) => {
		console.log('radio checked', e.target.value);
		this.setState({
			value3: e.target.value,
		});
	}
	onChange4 = (e) => {
		console.log('radio checked', e.target.value);
		this.setState({
			value4: e.target.value,
		});
	}

	render () {
		const { startValue, endValue, endOpen } = this.state;
		return (
			<div id="archive" className="mar-top-70">
				<Side active_link="archive" />
				<Head/>
				<Layout className="mar-top-72">
					<Content className="contains">
						<Row className="rows" type="flex" justify="space-around">
							<Col className="cols label" span={4}>Select City</Col>
							<Col className="cols option" span={12}>
								<Select
								className="select-icon"
								mode="tags"
								style={{ width: '70%' }}
								placeholder="Please Select City"
								onChange={handleChange}
								>
									{city}
								</Select>
							</Col>
						</Row>

						<Row className="rows" type="flex" justify="space-around">
							<Col className="cols label" span={4}>Select Device</Col>
							<Col className="cols option" span={12}>
								<Select
								className="select-icon"
								mode="tags"
								style={{ width: '70%' }}
								placeholder="Please Select Device"
								onChange={handleChange}
								>
									{device}
								</Select>
							</Col>
						</Row>

						<Row className="rows" type="flex" justify="space-around">
							<Col className="cols label" span={4}>Data Type</Col>
							<Col className="cols option" span={12}>
								<RadioGroup onChange={this.onChange1} value={this.state.value1}>
									<Radio value={1}>Summary</Radio>
									<Radio value={2}>Raw</Radio>
									<Radio value={3}>Average</Radio>
								</RadioGroup>
							</Col>
						</Row>

						<Row className="rows" type="flex" justify="space-around">
							<Col className="cols label" span={4}>Parameter</Col>
							<Col className="cols option" span={12}>
								<Row>
									<div className="label">
									<Checkbox>
										Select all
									</Checkbox>
									</div>
									<Row className="rows" type="flex" justify="space-around">
										<Col span={8}>
											<div className="label">AQI</div>
											<Checkbox
												className="selectall"
												indeterminate={this.state.indeterminate}
												onChange={this.onCheckAllChangeAqi}
												checked={this.state.checkAll}
											>
												Select all
											</Checkbox>
											<CheckboxGroup options={aqi} value={this.state.checkedList} onChange={this.onChangeAqi} />
										</Col>
										<Col span={8}>
											<div className="label">Weather</div>
											<Checkbox
												className="selectall"
												indeterminate={this.state.indeterminate}
												onChange={this.onCheckAllChangeWeather}
												checked={this.state.checkAll}
											>
												Select all
											</Checkbox>
											<CheckboxGroup options={weather} value={this.state.checkedList} onChange={this.onChangeWeather} />
										</Col>
										<Col span={8}>
											<div className="label">Other</div>
											<Checkbox
												className="selectall"
												indeterminate={this.state.indeterminate}
												onChange={this.onCheckAllChangeOther}
												checked={this.state.checkAll}
											>
												Select all
											</Checkbox>
											<CheckboxGroup options={other} value={this.state.checkedList} onChange={this.onChangeOther} />
										</Col>
									</Row>
								</Row>
							</Col>
						</Row>

						<Row className="rows" type="flex" justify="space-around">
							<Col className="cols label" span={4}>Time Interval</Col>
							<Col className="cols option" span={12}>
								<Row type="flex">
									<Col className="option" span={9}>
									<DatePicker
										disabledDate={this.disabledStartDate}
										showTime
										format="YYYY-MM-DD HH:mm:ss"
										value={startValue}
										placeholder="From Date "
										onChange={this.onStartChange}
										onOpenChange={this.handleStartOpenChange}
									/>
								</Col>
								<Col span={2} className="option label">__</Col>
								<Col className="option" span={10}>
									<DatePicker
										disabledDate={this.disabledEndDate}
										showTime
										format="YYYY-MM-DD HH:mm:ss"
										value={endValue}
										placeholder="To Date"
										onChange={this.onEndChange}
										open={endOpen}
										onOpenChange={this.handleEndOpenChange}
									/>
								</Col>
								</Row>
							</Col>
						</Row>

						<Row className="rows" type="flex" justify="space-around">
							<Col className="cols label" span={4}>Data Type</Col>
							<Col className="cols option" span={12}>
								<RadioGroup onChange={this.onChange2} value={this.state.value2}>
									<Radio value={1}>Grid</Radio>
									<Radio value={2}>Raw</Radio>
								</RadioGroup>
							</Col>
						</Row>

						<Row className="rows" type="flex" justify="space-around">
							<Col className="cols label" span={4}>Conversion Type</Col>
							<Col className="cols option" span={12}>
								<RadioGroup onChange={this.onChange3} value={this.state.value3}>
									<Radio value={1}>USEPA</Radio>
									<Radio value={2}>NAQI</Radio>
								</RadioGroup>
							</Col>
						</Row>

						<Row className="rows" type="flex" justify="space-around">
							<Col className="cols label" span={4}>Download Format</Col>
							<Col className="cols option" span={12}>
								<RadioGroup onChange={this.onChange3} value={this.state.value3}>
									<Radio value={1}>Excel</Radio>
									<Radio value={2}>Pdf</Radio>
								</RadioGroup>
							</Col>
						</Row>
					</Content>
					<Content className="contain">
						<Button className="button" type="primary" size="large">View</Button>
						<Button className="button" type="primary" icon="download" size="large">Download</Button>
					</Content>
				</Layout>
			</div>
		);
	}
}

export default Archive;